﻿using System.Configuration;
using UmBazo.Infrastructure.Examine.Configuration.SearchIndexSet.Collections;

namespace UmBazo.Infrastructure.Examine.Configuration.SearchIndexSet
{
    public class SearchIndexElement : ConfigurationElement
    {
        [ConfigurationProperty("Name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["Name"]; }
            set { this["Name"] = value; }
        }

        [ConfigurationProperty("ExcludeIndexFields")]
        public ExcludeIndexFieldsElementCollection ExcludeIndexFields
        {
            get { return (ExcludeIndexFieldsElementCollection)this["ExcludeIndexFields"]; }
            set { this["ExcludeIndexFields"] = value; }
        }
    }
}
