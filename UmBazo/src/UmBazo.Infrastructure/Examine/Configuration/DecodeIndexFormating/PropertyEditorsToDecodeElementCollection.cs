﻿using System.Configuration;

namespace UmBazo.Infrastructure.Examine.Configuration.DecodeIndexFormating
{
    [ConfigurationCollection(typeof(PropertyEditorsToDecodeElement))]
    public class PropertyEditorsToDecodeElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PropertyEditorsToDecodeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PropertyEditorsToDecodeElement)element).PropertyEditorAlias;
        }
    }
}