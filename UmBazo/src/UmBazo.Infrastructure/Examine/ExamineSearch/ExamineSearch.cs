﻿using System;
using System.Linq;
using System.Text;
using UmBazo.Infrastructure.Examine.ExamineSearch.Abstraction;
using UmBazo.Infrastructure.Examine.ExamineSearch.Model;
using Examine;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Umbraco.Web;

namespace UmBazo.Infrastructure.Examine.ExamineSearch
{
    public class ExamineSearch : IExamineSearch
    {
        public UmbracoHelper Umbraco { get; }

        public ExamineSearch(UmbracoHelper umbracoHelper)
        {
            Umbraco = umbracoHelper;
        }

        protected Query WhildCardQuery(string term, string[] fields, Lucene.Net.Util.Version version = null, Analyzer analyzer = null)
        {
            const string whildCardQueryFormat = "{0}:{1}* ";

            if (version == null)
            {
                version = Lucene.Net.Util.Version.LUCENE_29;
            }

            if (analyzer == null)
            {
                analyzer = new StandardAnalyzer(version);
            }

            term = QueryParser.Escape(term);

            var parser = new MultiFieldQueryParser(version, fields, analyzer);
            parser.SetDefaultOperator(QueryParser.OR_OPERATOR);

            var query = new StringBuilder();

            foreach (var field in fields)
            {
                query.AppendFormat(whildCardQueryFormat, field, term);
            }

            var queryString = query.ToString();

            queryString = queryString.Trim();

            var queryResult = parser.Parse(queryString);

            return queryResult;
        }
        
        public IExamineSearchResult Search(string query, string searchProviderName, string indexSet, string[] fieldFilter = null)
        {
            using (var stopWatch = new AutoStopwatch())
            {
                ISearchResults searchResults;
                
                if (fieldFilter == null || !fieldFilter.Any())
                {
                    searchResults = ExamineManager.Instance
                        .SearchProviderCollection[searchProviderName]
                        .Search(query, true);

                    return new ExamineSearchResult(searchResults, stopWatch.Elapsed);
                }

                var criteria = ExamineManager.Instance
                    .SearchProviderCollection[searchProviderName]
                    .CreateSearchCriteria();
                
                var finalQuery = new BooleanQuery();

                var terms = query.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var term in terms)
                {
                    var wildcardQuery = WhildCardQuery(term, fieldFilter);
                    finalQuery.Add(wildcardQuery, BooleanClause.Occur.SHOULD);
                }

                var lucineQuery = finalQuery.ToString();

                var filter = criteria
                    .RawQuery(lucineQuery);

                searchResults = ExamineManager.Instance
                    .SearchProviderCollection[searchProviderName]
                    .Search(filter);

                return new ExamineSearchResult(searchResults, stopWatch.Elapsed);
            }
        }
    }
}