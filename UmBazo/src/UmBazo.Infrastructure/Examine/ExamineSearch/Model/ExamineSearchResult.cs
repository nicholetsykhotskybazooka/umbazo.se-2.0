﻿using System;
using UmBazo.Infrastructure.Examine.ExamineSearch.Abstraction;
using Examine;

namespace UmBazo.Infrastructure.Examine.ExamineSearch.Model
{
    public class ExamineSearchResult : IExamineSearchResult
    {
        public ISearchResults Results { get; set; }
        public TimeSpan SearchTime { get; set; }

        public ExamineSearchResult()
        {
        }

        public ExamineSearchResult(ISearchResults results, TimeSpan searchTime)
        {
            Results = results;
            SearchTime = searchTime;
        }
    }
}
