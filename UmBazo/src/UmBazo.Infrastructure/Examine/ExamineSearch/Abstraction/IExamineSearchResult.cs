﻿using System;
using Examine;

namespace UmBazo.Infrastructure.Examine.ExamineSearch.Abstraction
{
    public interface IExamineSearchResult
    {
        ISearchResults Results { get; set; }
        TimeSpan SearchTime { get; set; }
    }
}
