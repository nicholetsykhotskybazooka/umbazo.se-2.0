﻿namespace UmBazo.Infrastructure.Examine.ExamineSearch.Abstraction
{
    public interface IExamineSearch
    {
        IExamineSearchResult Search(string searchTerm, string searchProviderName, string indexSet, string[] fieldFilter);
    }
}
