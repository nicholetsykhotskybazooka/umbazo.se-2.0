﻿using System.Linq;
using System.Web;
using Examine;
using umbraco;
using Umbraco.Web;

namespace UmBazo.Infrastructure.Examine.Model
{
    public class ExamineContentResult : ResultBase
    {
        public ExamineContentResult(UmbracoHelper helper) : base(helper)
        {
        }

        public override void Format(SearchResult result)
        {
            AddProperty("url", library.NiceUrl(result.Id));
            AddProperty("name", result.Fields["nodeName"]);
            
            //TODO Add right textfield
            if (result.Fields.ContainsKey("text"))
            {
                var text = result.Fields["text"].Take(50).ToString();
                AddProperty("text", HttpUtility.HtmlDecode(text));
            }
            AddProperty("type", "content");
        }

        protected string StripTagsCharArray(string html)
        {    
	        var array = new char[html.Length];
	        var arrayIndex = 0;
	        var inside = false;

            for (var i = 0; i < html.Length; i++)
	        {
                char let = html[i];

	            if (let == '<')
	            {
		            inside = true;
		            continue;
	            }

	            if (let == '>')
	            {
		            inside = false;
		            continue;
	            }

	            if (!inside)
	            {
		            array[arrayIndex] = let;
		            arrayIndex++;
	            }
	        }
	        return new string(array, 0, arrayIndex);    
        }
    }
}
