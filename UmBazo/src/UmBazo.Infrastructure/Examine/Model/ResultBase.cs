﻿using System.Collections.Generic;
using Examine;
using Umbraco.Web;
using UmBazo.Infrastructure.Examine.Abstraction;

namespace UmBazo.Infrastructure.Examine.Model
{
    public abstract class ResultBase : Dictionary<string, object>, IModelResult
    {
        public UmbracoHelper Umbraco { get; private set; }

        protected ResultBase(UmbracoHelper helper)
        {
            Umbraco = helper;
        }

        public abstract void Format(SearchResult result);

        public void AddProperty(string name, object value)
        {
            if (ContainsKey(name))
            {
                this[name] = value;

                return;
            }

            Add(name, value);
        }

        public void RemoveProperty(params string[] names)
        {
            foreach (var name in names)
            {
                if (ContainsKey(name))
                {
                    Remove(name);
                }
            }
        }
    }
}