﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UmBazo.Infrastructure.Examine.ExamineSearch.Abstraction;
using UmBazo.Infrastructure.Examine.Extensions;
using Examine.LuceneEngine.Config;
using Umbraco.Web;
using UmBazo.Core.SharedKernel;
using UmBazo.Infrastructure.Examine;

namespace UmBazo.Infrastructure.Examine
{
    public class ExamineSearchService : ISearchService
    {
        public IEnumerable<object> LookForContent(string query, string searchProviderName, string indexSet, CultureInfo culture = null)
        {
            if (string.IsNullOrEmpty(query))
            {
                return Enumerable.Empty<IPage>();
            }

            if (culture != null)
            {
                string cultIndexSet = string.Format("External_{0}_IndexSet", culture.Name);
                IndexSet indexItem = IndexSets.Instance.Sets.Cast<IndexSet>().FirstOrDefault(x => x.SetName == cultIndexSet);
                if (indexItem != null)
                {
                    indexSet = cultIndexSet;
                    searchProviderName = string.Format("External_{0}_Searcher", culture.Name);
                }
            }

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var examineSearch = new ExamineSearch.ExamineSearch(umbracoHelper);

            string[] fieldFilter = UmbExamineConfig.Instance.GetIndexFields(indexSet, new[] { "searchHide" });
            IExamineSearchResult searchResults = examineSearch.Search(query, searchProviderName, indexSet, fieldFilter);

            return searchResults.Results.Select(x => x.TryGetContentOrMedia(umbracoHelper));
        }
    }
}