﻿using System;
using System.Diagnostics;

namespace UmBazo.Infrastructure.Examine
{
    public class AutoStopwatch : Stopwatch, IDisposable
    {
        public AutoStopwatch()
        {
            Start();
        }

        public void Dispose()
        {
           Stop();
        }
    }
}