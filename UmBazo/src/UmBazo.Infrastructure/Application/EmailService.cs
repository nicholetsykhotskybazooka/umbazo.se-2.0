﻿using System.Net.Mail;
using UmBazo.Core.SharedKernel;

namespace UmBazo.Infrastructure.Application
{
    public class EmailService : IEmailService
    {
        public void SendEmail(string email, string subject, string message)
        {
            var mail = new MailMessage();
            mail.To.Add(email);
            var client = new SmtpClient();
            mail.Subject = subject;
            mail.Body = message;
            client.Send(mail);
        }
    }
}