﻿namespace UmBazo.Core.SharedKernel
{
    public interface IPage
    {
        bool SeoNoIndex { get; }
        bool SeoNoFollow { get; }
        //ISite Site { get; }
    }
}