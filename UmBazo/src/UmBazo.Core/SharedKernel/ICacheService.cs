﻿using System;

namespace UmBazo.Core.SharedKernel
{
    public interface ICacheService
    {
        T Add<T>(string key, T value, TimeSpan cacheTimeOut) where T : class;

        T Get<T>(string key) where T : class;

        T Put<T>(string key, Func<object, T> invalidateFunc, object state, TimeSpan expiration) where T : class;

        void Remove(string key);
    }
}