﻿namespace UmBazo.Core.SharedKernel
{
    public interface IOutputCacheManager
    {
        object ClearAllPageOutputCache();
    }
}
