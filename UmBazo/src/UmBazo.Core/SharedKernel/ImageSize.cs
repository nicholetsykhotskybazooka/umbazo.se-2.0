﻿namespace UmBazo.Core.SharedKernel
{
    public class ImageSize
    {
        public int Height { get; set; }
        public int Width { get; set; }

        public ImageSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}
