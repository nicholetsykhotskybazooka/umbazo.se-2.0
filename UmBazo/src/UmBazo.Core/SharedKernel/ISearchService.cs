﻿using System.Collections.Generic;
using System.Globalization;

namespace UmBazo.Core.SharedKernel
{
    public interface ISearchService
    {
        IEnumerable<object> LookForContent(string query, string searchProviderName, string indexSet, CultureInfo culture = null);
    }
}
