﻿namespace UmBazo.Core.SharedKernel
{
    public interface ITeaserAppearance
    {
        string View { get; }
        string Heading { get; }
        string Text { get; }
        int? Image { get; }
        string LinkText { get; }
        string LinkUrl { get; }
    }
}
