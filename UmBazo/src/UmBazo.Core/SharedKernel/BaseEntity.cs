﻿using System.Collections.Generic;

namespace UmBazo.Core.SharedKernel
{
    public abstract class BaseEntity
    {
        public List<BaseDomainEvent> Events = new List<BaseDomainEvent>();
    }
}
