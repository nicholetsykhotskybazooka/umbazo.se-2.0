﻿namespace UmBazo.Core.SharedKernel
{
    public interface IHandle<T> where T : BaseDomainEvent
    {
        void Handle(T domainEvent);
    }
}
