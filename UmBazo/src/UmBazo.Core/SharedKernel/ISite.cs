﻿namespace UmBazo.Core.SharedKernel
{
    public interface ISite
    {
        string AnalyticsTrackingID { get; }
        string GoogleMapsApiKey { get; }
        string SiteName { get; }
        string SiteDescription { get; }
        string CondominiumsEndpoint { get; }
        string HousesEndpoint { get; }
        string PlotsEndpoint { get; }
        string MapEndpoint { get; }
    }
}
