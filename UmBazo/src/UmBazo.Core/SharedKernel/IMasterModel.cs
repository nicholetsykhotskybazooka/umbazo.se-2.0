﻿namespace UmBazo.Core.SharedKernel
{
    public interface IMasterModel
    {
        string PageTitle { get; set; }
        string MetaDescription { get; set; }
        string TeaserHeading { get; set; }
        string TeaserPreamble { get; set; }
        //ISite Site { get; set; }
        string ContentType { get; set; }
        string DomainName { get; set; }
        string LanguageIsoCode { get; set; }
        string OGType { get; set; }
        string OGTitle { get; set; }
        string OGDescription { get; set; }
        int OGMedia { get; set; }
        int CardMedia { get; set; }
        string TwitterHandle { get; set; }
        string ProjectThemeColor { get; set; }
    }
}
