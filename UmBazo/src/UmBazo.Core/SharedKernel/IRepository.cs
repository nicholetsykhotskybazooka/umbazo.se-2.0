﻿using System.Collections.Generic;

namespace UmBazo.Core.SharedKernel
{
    public interface IRepository<T> where T : BaseEntity
    {
        List<T> List();
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
    }
}
