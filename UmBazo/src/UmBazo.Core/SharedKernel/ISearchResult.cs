﻿namespace UmBazo.Core.SharedKernel
{
    public interface ISearchResult
    {
        int PageId { get; set; }
        string PageTitle { get; set; }
        string Summery { get; set; }
        int? PageImage { get; set; }
    }
}
