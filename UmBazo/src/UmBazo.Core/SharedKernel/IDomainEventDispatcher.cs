﻿namespace UmBazo.Core.SharedKernel
{
    public interface IDomainEventDispatcher
    {
        void Dispatch(BaseDomainEvent domainEvent);
    }
}
