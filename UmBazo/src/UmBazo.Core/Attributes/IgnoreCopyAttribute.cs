﻿using System;

namespace UmBazo.Core.Attributes
{
    /// <summary>
    /// Ignore deep copy of property. 
    /// </summary>
    public class IgnoreCopyAttribute : Attribute
    {
    }
}
