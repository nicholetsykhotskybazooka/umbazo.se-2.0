﻿using System;

namespace UmBazo.Core.Attributes
{
    /// <summary>
    /// Attribute to decorate a property to determine if it's searchable.
    /// </summary>
    public class SearchableAttribute : Attribute
    {
        public bool Searchable { get; set; }
    }
}
