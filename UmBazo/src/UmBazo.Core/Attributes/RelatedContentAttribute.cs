﻿using System;

namespace UmBazo.Core.Attributes
{
    /// <summary>
    /// Indicate that the property has a relationship with other content. Used for invalidating the cache.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RelatedContentAttribute : Attribute
    {
        public string Name { get; private set; }

        public RelatedContentAttribute(string name)
        {
            Name = name;
        }
    }
}
