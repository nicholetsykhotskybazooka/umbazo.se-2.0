﻿using System;
using System.Globalization;

namespace UmBazo.Core.Extensions
{
    public static class NumberExtensions
    {
        public static string ToSek(this double value, bool displayCurrency = true)
        {
            return ((int)Math.Ceiling(value)).ToSek(displayCurrency);
        }

        public static string ToSek(this int value, bool displayCurrency = true)
        {
            var culture = new CultureInfo("sv-SE");
            culture.NumberFormat.CurrencyGroupSeparator = " ";
            culture.NumberFormat.CurrencyDecimalDigits = 0;

            if (!displayCurrency)
            {
                culture.NumberFormat.CurrencySymbol = string.Empty;
            }

            return value.ToString("C", culture);
        }

        public static string ToSquareMeter(this double value)
        {
            return value + " kvm";
        }

        public static string ToSquareMeter(this int value)
        {
            return ((double) value).ToSquareMeter();
        }
    }
}
