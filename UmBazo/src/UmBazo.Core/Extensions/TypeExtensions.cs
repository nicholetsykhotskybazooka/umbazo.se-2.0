﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using UmBazo.Core.SharedKernel;

namespace UmBazo.Core.Extensions
{
    public static class TypeExtensions
    {
        public static string GetDisplayValue<T>(this T item, Expression<Func<T, object>> propertyExpression)
        {
            if (item == null)
            {
                return default(string);
            }

            MemberInfo memberInfo = GetPropertyInformation(propertyExpression.Body);

            if (memberInfo == null)
            {
                throw new ArgumentException("No property reference expression was found.", "propertyExpression");
            }

            return typeof(T).GetProperty(memberInfo.Name).GetDisplayValue();

        }

        public static MemberInfo GetPropertyInformation(Expression propertyExpression)
        {
            Guard.Against.Null(propertyExpression, nameof(propertyExpression));

            var memberExpr = propertyExpression as MemberExpression;
            if (memberExpr == null)
            {
                var unaryExpr = propertyExpression as UnaryExpression;
                if (unaryExpr != null && unaryExpr.NodeType == ExpressionType.Convert)
                {
                    memberExpr = unaryExpr.Operand as MemberExpression;
                }
            }

            if (memberExpr != null && memberExpr.Member.MemberType == MemberTypes.Property)
            {
                return memberExpr.Member;
            }

            return null;
        }

        public static string GetDisplayValue(this PropertyInfo value)
        {
            if (!(value.GetCustomAttributes(typeof(DisplayAttribute), false) is DisplayAttribute[] descriptionAttributes))
            {
                return string.Empty;
            }

            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }

        public static bool HasDisplayValue(this PropertyInfo value)
        {
            string displayValue = value.GetDisplayValue();
            return displayValue != string.Empty && displayValue != value.ToString();
        }
    }
}
