﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace UmBazo.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToUpperInvariantWithNullCheck(this string value)
        {
            return value != null ? value.ToUpperInvariant() : "";
        }

        public static string ToRoomSummary(this string value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            string rok = "rum och kök.";

            if (value.Contains(rok))
            {
                return value.Replace(rok, "rok");
            }

            return value;
        }

        public static string Paragraph(this string value)
        {
            if (value.StartsWith("<p>"))
            {
                return value;
            }

            return "<p>" + value + "</p>";
        }

        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        /// <summary>
		/// Shortcut for string.IsNullOrEmpty.
		/// </summary>
		public static string IsNullOrEmpty(this String value, params object[] arg0)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : string.Format(value, arg0);
        }

        /// <summary>
		/// Shortcut for string.Format.
		/// </summary>
		public static string FormatString(this String value, params object[] arg0)
        {
            return string.IsNullOrWhiteSpace(value) ? string.Empty : string.Format(value, arg0);
        }

        public static string Sanitize(this string value, bool encode = true, bool stripTags = true, bool newLineToBr = true, int maxLength = 0)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            if (stripTags)
            {
                value = Regex.Replace(value, "<[^>]+>", "", RegexOptions.IgnoreCase);
            }

            if (encode)
            {
                value = WebUtility.HtmlEncode(value);
            }

            if (newLineToBr)
            {
                if (value.IndexOf("\n") > -1)
                {
                    value = value.Substring(0, value.LastIndexOf("\n"));
                }

                value = value.Replace("\n", "<br>");
            }

            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            if (maxLength <= 0 || maxLength >= value.Length)
            {
                return value;
            }

            value = value.Substring(0, maxLength - 3);

            if (!Regex.IsMatch(value, "(\\.|!|\\?)$"))
            {
                value += "...";
            }

            return value;
        }

        public static string SanitizeXmlString(this string xml)
        {
            if (xml == null)
            {
                return string.Empty;
            }

            var buffer = new StringBuilder(xml.Length);

            foreach (char c in xml)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
            }

            return buffer.ToString();
        }

        public static bool IsLegalXmlChar(int character)
        {
            return
            (
                character == 0x9 /* == '\t' == 9   */          ||
                character == 0xA /* == '\n' == 10  */          ||
                character == 0xD /* == '\r' == 13  */          ||
                (character >= 0x20 && character <= 0xD7FF) ||
                (character >= 0xE000 && character <= 0xFFFD) ||
                (character >= 0x10000 && character <= 0x10FFFF)
            );
        }

        public static int? ParseIntOrDefault(this string value)
        {
            int result = 0;
            return int.TryParse(value, out result) ? result : (int?)null;
        }

        public static bool? ParseBoolOrDefault(this string value)
        {
            bool result = false;
            return bool.TryParse(value, out result) ? result : (bool?)null;
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        /// <summary>
		/// Shortcut for value == string.Empty.
		/// </summary>
		public static bool IsStringEmpty(this string value)
        {
            return value == string.Empty;
        }

        /// <summary>
		/// Shortcut for string.IsNullOrEmpty.
		/// </summary>
		public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
		/// Shortcut for !string.IsNullOrEmpty.
		/// </summary>
		public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
		/// From Twitterizer http://www.twitterizer.net/. Used for oauth signatures.
		/// </summary>
		public static string EncodeRFC3986(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            string encoded = Uri.EscapeDataString(value);

            return Regex
                .Replace(encoded, "(%[0-9a-f][0-9a-f])", c => c.Value.ToUpper())
                .Replace("(", "%28")
                .Replace(")", "%29")
                .Replace("$", "%24")
                .Replace("!", "%21")
                .Replace("*", "%2A")
                .Replace("'", "%27")
                .Replace("%7E", "~");
        }

        /// <summary>
		/// Truncates the string to a specified length and replace the truncated with a suffix.
		/// </summary>
		public static string Truncate(this string value, int maxLength, string suffix = "...")
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            if (maxLength <= 0)
            {
                return value;
            }

            int strLength = maxLength - suffix.Length;

            if (strLength <= 0)
            {
                return value;
            }

            if (value.Length <= maxLength)
            {
                return value;
            }

            value = value.Substring(0, strLength);
            value = value.TrimEnd();
            value += suffix;

            return value;
        }

        // <summary>
        /// Combine two strings.
        /// </summary>
        public static string CombineWith(this string input, string suffix, string separator = " ")
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.IsNullOrEmpty(suffix) ? string.Empty : suffix;
            }

            return string.IsNullOrEmpty(suffix) ? input : string.Format("{0}{1}{2}", input, separator, suffix);
        }
    }
}
