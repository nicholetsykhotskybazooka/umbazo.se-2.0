﻿using System;

namespace UmBazo.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime Next(this DateTime from, DayOfWeek dayOfWeek)
        {
            int start = (int)from.DayOfWeek;
            int target = (int)dayOfWeek;

            if (target <= start)
            {
                target += 7;
            }

            return from.AddDays(target - start);
        }

        public static DateTime AddWeek(this DateTime value)
        {
            return value.AddDays(7);
        }

        public static DateTime? MinToNullable(this DateTime value)
        {
            return value == DateTime.MinValue ? null : new DateTime?(value);
        }
    }
}
