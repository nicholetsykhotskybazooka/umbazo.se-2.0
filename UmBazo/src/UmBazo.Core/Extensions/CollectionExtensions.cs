﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UmBazo.Core.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsNotNullAndAny<TSource>(this IEnumerable<TSource> source)
        {
            return source.IsNotNull() && source.Any();
        }

        public static void AddIf<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, bool condition, Func<Tuple<TKey, TValue>> func)
        {
            if (condition)
            {
                Tuple<TKey, TValue> item = func();
                dictionary.Add(item.Item1, item.Item2);
            }
        }
    }
}
