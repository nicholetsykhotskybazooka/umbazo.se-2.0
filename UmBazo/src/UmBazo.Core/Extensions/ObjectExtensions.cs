﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UmBazo.Core.Attributes;

namespace UmBazo.Core.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Cast object to...
        /// </summary>
        public static T As<T>(this object obj) where T : class
        {
            return obj as T;
        }

        /// <summary>
        /// Checks an object to see if it is null.
        /// </summary>
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }

        /// <summary>
        /// Checks an object to see if it is NOT null.
        /// </summary>
        public static bool IsNotNull(this object obj)
        {
            return obj != null;
        }

        public static T CopyFrom<T>(this T toObject, object fromObject)
        {
            Type fromObjectType = fromObject.GetType();

            foreach (PropertyInfo toProperty in toObject.GetType().GetProperties())
            {
                PropertyInfo fromProperty = fromObjectType.GetProperty(toProperty.Name);

                if (fromProperty != null) 
                {
                    Type fromType = Nullable.GetUnderlyingType(fromProperty.PropertyType) ?? fromProperty.PropertyType;
                    Type toType = Nullable.GetUnderlyingType(toProperty.PropertyType) ?? toProperty.PropertyType;

                    if (toType.IsAssignableFrom(fromType))
                    {
                        MethodInfo setMethod = toProperty.GetSetMethod();

                        if (setMethod != null)
                        {
                            if (!Attribute.IsDefined(fromProperty, typeof(IgnoreCopyAttribute)))
                            {
                                toProperty.SetValue(toObject, fromProperty.GetValue(fromObject, null), null);
                            }
                        }
                    }
                }
            }

            return toObject;
        }

        public static bool IsCollectionType(this Type type)
        {
            if (!type.GetGenericArguments().Any())
                return false;

            Type genericTypeDefinition = type.GetGenericTypeDefinition();
            Type[] collectionTypes = new[] { typeof(IEnumerable<>), typeof(ICollection<>), typeof(IList<>), typeof(List<>) };

            return collectionTypes.Any(x => x.IsAssignableFrom(genericTypeDefinition));
        }
    }
}
