﻿namespace UmBazo.Core.Contracts
{
    public interface IContactPerson
    {
        string Contactname { get; }
        string Phone { get; }
        int Image { get; }
        string Email { get; }
        string Company { get; }
    }
}
