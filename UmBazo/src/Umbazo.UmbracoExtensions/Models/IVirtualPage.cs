﻿using Umbraco.Core.Models;

namespace UmBazo.UmbracoExtensions.Models
{
    public interface IVirtualPage
    {
        T Convert<T>(IPublishedContent content) where T : class;
    }
}