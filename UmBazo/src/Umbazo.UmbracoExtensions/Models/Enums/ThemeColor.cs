﻿using System.ComponentModel;

namespace UmBazo.UmbracoExtensions.Models.Enums
{
    public enum ThemeColor
    {
        [Description("Vit")]
        White = 0,
        [Description("Rosa")]
        Pink = 1,
        [Description("Beige")]
        Beige = 2,
        [Description("Gul")]
        Yellow = 3,
        [Description("Grön")]
        Green = 4,
        [Description("Blå")]
        Blue = 5,
        [Description("Grå")]
        Grey = 6
    }
}
