﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace UmBazo.UmbracoExtensions.Models
{
    public static class IPublishedContentExtensions
    {
        public static IEnumerable<IPublishedContent> FilterPublished(this IEnumerable<IPublishedContent> content, UmbracoHelper helper)
        {
            foreach (IPublishedContent item in content)
            {
                IPublishedContent typed = helper.TypedContent(item.Id);

                if (typed != null)
                {
                    yield return typed;
                }
            }
        }

        public static string GetUrlWithDomainPrefix(this IPublishedContent content)
        {
            string url = content.Url;

            if (url.StartsWith("/"))
            {
                url = url.Substring(1);
            }

            string domainPrefix = string.Format("https://{0}/", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);

            return url.StartsWith(domainPrefix) ? url : domainPrefix + url;
        }

        public static bool HasChildren(this IPublishedContent content)
        {
            return content.Children().Any(x => x.IsVisible());
        }
    }
}