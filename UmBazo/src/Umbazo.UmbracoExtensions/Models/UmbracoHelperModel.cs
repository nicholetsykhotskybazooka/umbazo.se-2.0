﻿using Umbraco.Web;

namespace UmBazo.UmbracoExtensions.Models
{
    public class UmbracoHelperModel<T> where T : class
    {
        public UmbracoHelper Umbraco { get; private set; }

        public T Content { get; private set; }

        public UmbracoHelperModel(UmbracoHelper umbraco, T content)
        {
            Umbraco = umbraco;
            Content = content;
        }
    }
}
