﻿using Umbraco.Core.Models;
using Umbraco.Web.Models;
using UmBazo.Core.SharedKernel;

namespace UmBazo.UmbracoExtensions.Models
{
    public class MasterModel<T> : RenderModel<T>, IMasterModel where T : class, IPublishedContent
    {
        public MasterModel(T content) : base(content) { }
        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }
        public string TeaserHeading { get; set; }
        public string TeaserPreamble { get; set; }
        public string ContentType { get; set; }
        public string DomainName { get; set; }
        public string LanguageIsoCode { get; set; }
        public string OGType { get; set; }
        public string OGTitle { get; set; }
        public string OGDescription { get; set; }
        public int OGMedia { get; set; }
        public int CardMedia { get; set; }
        public string TwitterHandle { get; set; }
        public string ProjectThemeColor { get; set; }
    }
}