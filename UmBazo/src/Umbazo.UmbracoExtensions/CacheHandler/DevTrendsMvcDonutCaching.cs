﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using DevTrends.MvcDonutCaching;
using OutputCache = DevTrends.MvcDonutCaching.OutputCache;

namespace UmBazo.UmbracoExtensions.CacheHandler
{
    public class DevTrendsMvcDonutCaching : UmBazo.Core.SharedKernel.IOutputCacheManager
    {
        public object ClearAllPageOutputCache()
        {
            var enumerableCache = OutputCache.Instance as IEnumerable<KeyValuePair<string, object>>;

            if (enumerableCache == null)
            {
                return null;
            }

            // get all output cache keys that contain current page id
            IEnumerable<string> keysToDelete = enumerableCache
                .Where(x => !string.IsNullOrEmpty(x.Key))
                .Select(x => x.Key);

            // remove all caches for current page
            foreach (string key in keysToDelete)
            {
                OutputCache.Instance.Remove(key);
            }

            return null;
        }
    }
}
