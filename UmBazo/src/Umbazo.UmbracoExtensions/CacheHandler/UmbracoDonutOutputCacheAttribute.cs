﻿using System.Web;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Umbraco.Web;

namespace UmBazo.UmbracoExtensions.CacheHandler
{
    public class UmbracoDonutOutputCacheAttribute: DonutOutputCacheAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Duration = -1;
            bool previewMode = UmbracoContext.Current.InPreviewMode;

            if (!previewMode)
            {
                if (UmbracoContext.Current.HttpContext.Request.QueryString["dtgePreview"] == "1")
                {
                    previewMode = true;
                }
            }

            if (previewMode || HttpContext.Current.IsDebuggingEnabled)
            {
                Duration = 0;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
