﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UmBazo.Core.Attributes;
using UmBazo.UmbracoExtensions.Extensions;
using DevTrends.MvcDonutCaching;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;
using Umbraco.Core.Sync;
using Umbraco.Web;
using Umbraco.Web.Cache;

namespace UmBazo.UmbracoExtensions.CacheHandler
{
    public class UmbracoCacheHandler: ApplicationEventHandler
    {
        private UmbracoHelper _umbracoHelper;

        public UmbracoHelper UmbracoHelper
        {
            get
            {
                if (_umbracoHelper == null)
                {
                    _umbracoHelper = UmbracoContext.Current.GetUmbracoHelper();
                }

                return _umbracoHelper;
            }
        }

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PageCacheRefresher.CacheUpdated += PageCacheRefresherCacheUpdated;
        }

        private void PageCacheRefresherCacheUpdated(PageCacheRefresher sender, CacheRefresherEventArgs e)
        {
            UmbracoContext.Current.EnsureUmbracoContext();
            IPublishedContent item = null;
            string doctype = string.Empty;

            switch (e.MessageType)
            {
                case MessageType.RefreshById:
                case MessageType.RemoveById:
                    int contentId = (int)e.MessageObject;
                    doctype = string.Empty;
                    item = UmbracoContext.Current.ContentCache.GetById(contentId);
                    if (item != null)
                    {
                        doctype = item.DocumentTypeAlias;
                    }
                    ClearPageOutputCache(contentId, item);
                    break;
                case MessageType.RefreshByInstance:
                    if (!(e.MessageObject is Content Refreshedcontent))
                    {
                        return;
                    }
                    item = UmbracoContext.Current.ContentCache.GetById(Refreshedcontent.Id);
                    if (item != null)
                    {
                        doctype = item.DocumentTypeAlias;
                    }
                    ClearPageOutputCache(Refreshedcontent.Id, item);
                    break;
                case MessageType.RemoveByInstance:
                    if (!(e.MessageObject is Content))
                    {
                        return;
                    }
                    ClearAllPageOutputCache();
                    break;
            }
        }

        private void ClearPageOutputCache(int contentId, IPublishedContent content)
        {
            var enumerableCache = OutputCache.Instance as IEnumerable<KeyValuePair<string, object>>;

            if (enumerableCache == null)
            {
                return;
            }

            // get all output cache keys that contain current page id
            var keysToDelete = enumerableCache
                .Where(x => !string.IsNullOrEmpty(x.Key) && x.Key.Contains($"pageid={contentId};"))
                .Select(x => x.Key).ToList();

            IPublishedContent root = UmbracoHelper.TypedContentAtRoot().FirstOrDefault();

            // delete page output cache for related content 
            ClearPageOutputCacheForReleatedContent(content, enumerableCache, keysToDelete, root);

            // remove all caches for current page
            foreach (string key in keysToDelete)
            {
                OutputCache.Instance.Remove(key);
            }

            var manager = new OutputCacheManager();

            if (content.Id == root.Id) // start page changes - update footer
            {
                manager.RemoveItems("Footer", "RenderFooter");
            }
        }

        private static void ClearPageOutputCacheForReleatedContent(IPublishedContent content, IEnumerable<KeyValuePair<string, object>> enumerableCache, List<string> keysToDelete, IPublishedContent root)
        {
            foreach (IPublishedContent child in root.Descendants())
            {
                IEnumerable<PropertyInfo> properties = child.GetType().GetProperties();

                foreach (PropertyInfo property in properties)
                {
                    object[] attrs = property.GetCustomAttributes(true);
                    foreach (object attr in attrs)
                    {
                        var relatedContentAttr = attr as RelatedContentAttribute;

                        if (relatedContentAttr == null)
                        {
                            continue;
                        }

                        string name = relatedContentAttr.Name;
                        bool hasRelatedContent = child.HasValue(name) && child.GetPropertyValue<IEnumerable<IPublishedContent>>(name).Contains(content);

                        if (hasRelatedContent)
                        {
                            keysToDelete.AddRange(enumerableCache.Where(x => !string.IsNullOrEmpty(x.Key) && x.Key.Contains($"pageid={child.Id};")).Select(x => x.Key).ToList());
                        }
                    }
                }
            }
        }

        private void ClearAllPageOutputCache()
        {
            var enumerableCache = OutputCache.Instance as IEnumerable<KeyValuePair<string, object>>;

            if (enumerableCache == null)
            {
                return;
            }

            // get all output cache keys that contain current page id
            IEnumerable<string> keysToDelete = enumerableCache
                .Where(x => !string.IsNullOrEmpty(x.Key))
                .Select(x => x.Key);

            // remove all caches for current page
            foreach (string key in keysToDelete)
            {
                OutputCache.Instance.Remove(key);
            }
        }
    }
}