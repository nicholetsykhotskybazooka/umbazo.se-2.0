﻿using UmBazo.Core.SharedKernel;
using Umbraco.Web.Mvc;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public abstract class RenderMvcControllerBase : RenderMvcController
    {
        private readonly ILoggingService _logger;

        public RenderMvcControllerBase(ILoggingService logger)
        {
            _logger = logger;
        }
    }
}