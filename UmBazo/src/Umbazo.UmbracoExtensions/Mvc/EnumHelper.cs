﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public class EnumHelper<T> where T : struct, IConvertible
    {
        public static T GetEnumByDescription(string description)
        {
            foreach (T e in Enum.GetValues(typeof(T)))
            {
                FieldInfo fieldInfo = e.GetType().GetField(e.ToString());
                var attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));
                if (attribute.Description == description)
                {
                    return e;
                }
            }
            return default(T);
        }
    }
}
