﻿using System.Collections.Generic;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public interface ITableData
    {
        string CustomCssClass { get; }
    }
}