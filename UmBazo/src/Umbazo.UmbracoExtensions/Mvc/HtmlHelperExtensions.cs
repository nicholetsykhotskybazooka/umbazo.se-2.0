﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebPages;
using UmBazo.Core.Extensions;
using UmBazo.Core.SharedKernel;
using Our.Umbraco.Picture;
using Our.Umbraco.Picture.Models;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public static class HtmlHelperExtensions
    {
        public const string LinkTemplate = "<a href=\"{0}\">{1}</a>";

        public static IHtmlString PartialIf(this HtmlHelper helper, string partialViewName, object model, bool condition)
        {
            return condition ? helper.Partial(partialViewName, model) : MvcHtmlString.Empty;
        }

        public static IHtmlString RenderPictureIfNotEmpty(this HtmlHelper helper, IPictureElement picture)
        {
            return picture == null ? MvcHtmlString.Empty : helper.RenderPicture(picture);
        }

        public static IHtmlString TableFor<T, TValue>(this HtmlHelper<T> html, Expression<Func<T, TValue>> prop, IEnumerable<ITableData> model, string tableClass)
        {
            Type modelType = model.GetType();
            Type valueType = modelType.GetGenericArguments()[0];
            PropertyInfo[] properties = valueType.GetProperties().Where(p => Attribute.IsDefined(p, typeof(DisplayAttribute))).ToArray();

            string headerColumns = string.Join(string.Empty, properties.Select(p => string.Format("<th>{0}</th>", p.GetDisplayValue())));

            var rows = new List<string>();

            foreach (ITableData row in model)
            {
                string rowTag = "<tr>{0}</tr>";
                string cells = string.Join(string.Empty, (from property in properties select string.Format("<td>{0}</td>", property.GetValue(row, null) != null ? property.GetValue(row, null).ToString() : string.Empty)));
                rows.Add(string.Format(rowTag, cells));
            }

            string rowsString = string.Join(string.Empty, rows);
            string returnFormat = "<table{0}><thead><tr>{1}</tr></thead><tbody>{2}</tbody></table>";
            string returnString = string.Format
                (
                    returnFormat,
                    (string.IsNullOrEmpty(tableClass) ? string.Empty : string.Format(" class=\"{0}\"", tableClass)),
                    headerColumns,
                    rowsString
                );

            return new MvcHtmlString(returnString);
        }

        public static IHtmlString ResponsiveTableFor<T, TValue>(this HtmlHelper<T> html, Expression<Func<T, TValue>> prop, IEnumerable<ITableData> model, string tableClass)
        {
            Type modelType = model.GetType();
            Type valueType = modelType.GetGenericArguments()[0];
            PropertyInfo[] properties = valueType.GetProperties().Where(p => Attribute.IsDefined(p, typeof(DisplayAttribute))).ToArray();

            string headerColumns = string.Join(string.Empty, properties.Select(p => string.Format("<div class=\"{0}__head__row__item\">{1}</div>", tableClass, p.GetDisplayValue())));

            var rows = new List<string>();

            foreach (ITableData row in model)
            {
                string rowTag = "<div class=\"{0}__list__block {1}\">{2}</div>";
                string cells = string.Join(string.Empty, (from property in properties where property.HasDisplayValue() select string.Format("<div class=\"{0}__list__block__item\"><span class=\"{0}__list__block__item__heading\">{1}</span><span class=\"{0}__list__block__item__value\">{2}</span></div>", tableClass, property.GetDisplayValue(), property.GetValue(row, null) != null ? property.GetValue(row, null).ToString() : string.Empty)));
                rows.Add(string.Format(rowTag, tableClass, row.CustomCssClass, cells));
            }

            string rowsString = string.Join(string.Empty, rows);
            string returnFormat = "<div class=\"{0}\"><div class=\"{0}__head\"><div class=\"{0}__head__row\" v-html=\"titles\">{1}</div></div><div class=\"{0}__list\" v-html=\"listData\">{2}</div></div>";
            string returnString = string.Format
            (
                returnFormat,
                (string.IsNullOrEmpty(tableClass) ? string.Empty : tableClass),
                headerColumns,
                rowsString
            );

            return new MvcHtmlString(returnString);
        }

        /// <summary>
		/// Delegated template rendering method with condition.
		/// </summary>
		public static IHtmlString RenderTemplate(this HtmlHelper helper, Func<HelperResult> itemTemplate = null, bool condition = true)
        {
            Guard.Against.Null(itemTemplate, nameof(itemTemplate));

            if (!condition)
            {
                return MvcHtmlString.Empty;
            }

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            if (itemTemplate != null)
            {
                itemTemplate().WriteTo(writer);
            }

            return new MvcHtmlString(buffer.ToString());
        }
    }
}