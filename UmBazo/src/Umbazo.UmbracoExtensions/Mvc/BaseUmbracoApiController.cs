﻿using System.Linq;
using UmBazo.Core.SharedKernel;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public abstract class BaseUmbracoApiController : UmbracoApiController
    {
        public ILoggingService Log { get; private set; }

        public IPublishedContent Root { get; private set; }

        public BaseUmbracoApiController(ILoggingService logger)
        {
            Log = logger;
            Root = Umbraco.TypedContentAtRoot().FirstOrDefault();
        }
    }
}