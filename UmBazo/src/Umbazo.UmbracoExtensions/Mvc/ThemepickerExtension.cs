﻿using UmBazo.UmbracoExtensions.Models.Enums;
using Umbraco.Core.PropertyEditors.ValueConverters;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public static class ThemePickerExtension
    {
        public static string ToThemeLogicalName(this ColorPickerValueConverter.PickedColor themeColor)
        {
            ThemeColor themeColorAndDescriptions = EnumHelper<ThemeColor>.GetEnumByDescription(themeColor.Label);
            return themeColorAndDescriptions.ToString().ToLower();
        }
    }
}
