﻿using System;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public class GridViewPage<T> : UmbracoViewPage<IPublishedContent> where T : IPublishedContent
    {
        private T _model;

        protected new T Model => _model != null ? _model : (_model = (T)Activator.CreateInstance(typeof(T), base.Model));

        public override void Execute()
        {
        }
    }
}