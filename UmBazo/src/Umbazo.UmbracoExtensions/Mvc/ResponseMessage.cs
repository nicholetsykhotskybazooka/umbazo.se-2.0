﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UmBazo.UmbracoExtensions.Mvc
{
    public class ResponseMessage<T>
    {
        public bool Success { get; set; }
        public IEnumerable<T> Result { get; set; }
        public string Message { get; set; }
        public int TotalItemCount { get; set; }
        public int PageCount { get; set; } = 1;
        public int PageNumber { get; set; } = 1;
        public DateTime Created { get; set; } = DateTime.Now;

        public void SetSuccessfulResult(IEnumerable<T> result, string message = "")
        {
            TotalItemCount = result.Count();
            Result = result.ToList();
            Success = true;
            Message = message;
        }

        public void SetFailedResult(Exception ex)
        {
            Message = ex.ToString();
            Success = false;
        }
    }
}