﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace UmBazo.UmbracoExtensions.Extensions
{
	public static class UmbracoHelperExtension
	{
		public static IPublishedContent GetHomePage(this UmbracoHelper src)
		{
			return src.TypedContentAtRoot().FirstOrDefault();
		}
	}
}
