﻿using UmBazo.Core.SharedKernel;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class ImageExtensions
    {
        public static bool IsNotNull(this int? id)
        {
            return id > 0 ? true : false;
        }

        public static string GetFullCropUrl(this ImageCropDataSet value, string alias)
        {
            return value != null && value.HasImage() && value.HasCrop(alias) ? value.Src + value.GetCropUrl(alias) : string.Empty;
        }

        public static string GetCropUrl(this IPublishedContent mediaItem, ImageSize size)
        {
            return mediaItem.GetCropUrl(size.Width, size.Height);
        }
    }
}
