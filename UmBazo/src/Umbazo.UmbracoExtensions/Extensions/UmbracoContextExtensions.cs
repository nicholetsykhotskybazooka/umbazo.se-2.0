﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.Security;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class UmbracoContextExtensions
    {
        public static string GetHostWithScheme(this UmbracoContext context)
        {
            HttpRequest request = HttpContext.Current.Request;

            return request.Url.Scheme + "://" + request.Url.Host;
        }

        public static bool IsIPAddressInRange(this UmbracoContext context, string[] range)
        {
            string ip = context.GetIPAddress();
            return range != null && range.IndexOf(ip) > -1;
        }

        public static string GetIPAddress(this UmbracoContext context)
        {
            string ipAddress = context.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');

                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static void EnsureUmbracoContext(this UmbracoContext context)
        {
            if (context == null)
            {
                var httpContext = new HttpContextWrapper(HttpContext.Current ?? new HttpContext(new SimpleWorkerRequest("/", string.Empty, new StringWriter())));

                UmbracoContext.EnsureContext(
                    httpContext,
                    ApplicationContext.Current,
                    new WebSecurity(httpContext, ApplicationContext.Current),
                    UmbracoConfig.For.UmbracoSettings(),
                    UrlProviderResolver.Current.Providers,
                    false);
            }
        }

        public static UmbracoHelper GetUmbracoHelper(this UmbracoContext context)
        {
            return new UmbracoHelper(context);
        }
    }
}