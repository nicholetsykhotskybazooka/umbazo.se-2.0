﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class SelectListItemExtensions
    {
        public static List<SelectListItem> ToSelectListItems(this Dictionary<string,string> dictionary)
        {
            var list = new List<SelectListItem>();

            list.AddRange(dictionary.Select(keyValuePair => new SelectListItem()
            {
                Value = keyValuePair.Key,
                Text = keyValuePair.Value
            }));
            if (list.Any())
            {
                list = list.OrderBy(x => x.Text).ToList();
            }

            return list;
        }
    }
}
