﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class ModelStateExtensions
    {
        public static List<string> GetModelErrorKeys(this ModelStateDictionary modelState)
        {
            var errorKeys = new List<string>();
            foreach (string modelStateKey in modelState.Keys)   
            {
                ModelState modelStateVal = modelState[modelStateKey];
                foreach (ModelError error in modelStateVal.Errors)
                {
                    string key = modelStateKey;
                    errorKeys.Add(key);
                }
            }
            return errorKeys;
        }
    }
}
