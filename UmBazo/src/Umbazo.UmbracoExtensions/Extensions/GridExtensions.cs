﻿using System.Linq;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class GridExtensions
    {
        public static bool IsNotEmpty(this Newtonsoft.Json.Linq.JToken gridJToken)
        {
            if (gridJToken.SelectTokens("sections[*].rows[*].areas[*].controls[*].value").Any())
            {
                return true;
            }
            return false;
        }
    }
}
