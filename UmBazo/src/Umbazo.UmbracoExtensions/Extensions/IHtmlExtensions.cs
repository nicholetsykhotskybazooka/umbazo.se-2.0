﻿using System.Web;

namespace UmBazo.UmbracoExtensions.Extensions
{
    public static class IHtmlExtensions
    {
        public static bool IsNotNullOrEmpty(this IHtmlString value)
        {
            return value != null && value.ToString() != string.Empty;
        }
    }
}
