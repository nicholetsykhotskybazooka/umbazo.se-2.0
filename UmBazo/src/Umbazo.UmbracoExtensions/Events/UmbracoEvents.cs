﻿using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace UmBazo.UmbracoExtensions.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        private static ServiceContext Services
        {
            get { return UmbracoContext.Current.Application.Services; }
        }

        protected static UmbracoHelper Umbraco
        {
            get { return new UmbracoHelper(UmbracoContext.Current); }
        }

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }
    }
}
