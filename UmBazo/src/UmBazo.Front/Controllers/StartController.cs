﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmBazo.Models;
using UmBazo.UmbracoExtensions.Models;

namespace UmBazo.Controllers
{
	public class StartController : RenderMvcController
	{
	    public ActionResult Index()
	    {
	        var master = ModelBuilder.CreateMasterModel() as MasterModel<Start>;
		    return CurrentTemplate(master);
		}
	}
}