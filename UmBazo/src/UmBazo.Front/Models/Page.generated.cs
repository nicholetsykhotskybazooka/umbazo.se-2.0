//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace UmBazo.Models
{
	// Mixin content Type 1060 with alias "page"
	/// <summary>Page</summary>
	public partial interface IPage : IPublishedContent
	{
		/// <summary>Twitterbild</summary>
		IPublishedContent CardImage { get; }

		/// <summary>Metabeskrivning</summary>
		string MetaDescription { get; }

		/// <summary>Beskrivning</summary>
		string OgDescription { get; }

		/// <summary>Bild</summary>
		IPublishedContent OgImage { get; }

		/// <summary>Titel</summary>
		string OgTitle { get; }

		/// <summary>Sidtitel</summary>
		string PageTitle { get; }

		/// <summary>Nofollow</summary>
		bool SeoNoFollow { get; }

		/// <summary>Noindex</summary>
		bool SeoNoIndex { get; }
	}

	/// <summary>Page</summary>
	[PublishedContentModel("page")]
	public partial class Page : PublishedContentModel, IPage
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "page";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Page(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Page, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Twitterbild: En unik bild för innehållet att visa vid delning som Twitter-card. Bildförhållander ska vara 2: 1 med minsta dimensioner på 300x157 eller högst 4096x4096 pixlar. Bilder måste vara mindre än 5 MB i storlek. JPG, PNG, WEBP och GIF-format stöds.
		///</summary>
		[ImplementPropertyType("cardImage")]
		public IPublishedContent CardImage
		{
			get { return GetCardImage(this); }
		}

		/// <summary>Static getter for Twitterbild</summary>
		public static IPublishedContent GetCardImage(IPage that) { return that.GetPropertyValue<IPublishedContent>("cardImage"); }

		///<summary>
		/// Metabeskrivning: Beskrivning av innehållet för sökmotorer.
		///</summary>
		[ImplementPropertyType("metaDescription")]
		public string MetaDescription
		{
			get { return GetMetaDescription(this); }
		}

		/// <summary>Static getter for Metabeskrivning</summary>
		public static string GetMetaDescription(IPage that) { return that.GetPropertyValue<string>("metaDescription"); }

		///<summary>
		/// Beskrivning: En sammanfattning av sidans innehåll med max 300 tecken. Om inget anges här visas sidans metabeskrivning istället och om även den är tom startsidans delningsbeskrivning.
		///</summary>
		[ImplementPropertyType("ogDescription")]
		public string OgDescription
		{
			get { return GetOgDescription(this); }
		}

		/// <summary>Static getter for Beskrivning</summary>
		public static string GetOgDescription(IPage that) { return that.GetPropertyValue<string>("ogDescription"); }

		///<summary>
		/// Bild: En unik bild för innehållet att visa vid delning på sociala medier som Facebook. Rekommenderad bildstorlek 1200 x 630.
		///</summary>
		[ImplementPropertyType("ogImage")]
		public IPublishedContent OgImage
		{
			get { return GetOgImage(this); }
		}

		/// <summary>Static getter for Bild</summary>
		public static IPublishedContent GetOgImage(IPage that) { return that.GetPropertyValue<IPublishedContent>("ogImage"); }

		///<summary>
		/// Titel: En kort titel för sidan. Om inget anges här används sidans titel som delningstitel.
		///</summary>
		[ImplementPropertyType("ogTitle")]
		public string OgTitle
		{
			get { return GetOgTitle(this); }
		}

		/// <summary>Static getter for Titel</summary>
		public static string GetOgTitle(IPage that) { return that.GetPropertyValue<string>("ogTitle"); }

		///<summary>
		/// Sidtitel: Sidans huvudrubrik.
		///</summary>
		[ImplementPropertyType("pageTitle")]
		public string PageTitle
		{
			get { return GetPageTitle(this); }
		}

		/// <summary>Static getter for Sidtitel</summary>
		public static string GetPageTitle(IPage that) { return that.GetPropertyValue<string>("pageTitle"); }

		///<summary>
		/// Nofollow: Säger till sökmotorn att inte följa några länkar på webbsidan.
		///</summary>
		[ImplementPropertyType("seoNoFollow")]
		public bool SeoNoFollow
		{
			get { return GetSeoNoFollow(this); }
		}

		/// <summary>Static getter for Nofollow</summary>
		public static bool GetSeoNoFollow(IPage that) { return that.GetPropertyValue<bool>("seoNoFollow"); }

		///<summary>
		/// Noindex: Talar om för sökmotorn att inte indexera webbsidan.
		///</summary>
		[ImplementPropertyType("seoNoIndex")]
		public bool SeoNoIndex
		{
			get { return GetSeoNoIndex(this); }
		}

		/// <summary>Static getter for Noindex</summary>
		public static bool GetSeoNoIndex(IPage that) { return that.GetPropertyValue<bool>("seoNoIndex"); }
	}
}
