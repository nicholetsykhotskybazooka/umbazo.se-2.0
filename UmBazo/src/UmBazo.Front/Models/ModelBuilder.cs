﻿using System;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using UmBazo.Core.SharedKernel;
using UmBazo.UmbracoExtensions.Models;

namespace UmBazo.Models
{
	public class ModelBuilder
	{
		public static IMasterModel CreateMasterModel()
		{
			return CreateMasterModel(UmbracoContext.Current.PublishedContentRequest.PublishedContent);
		}

		public static IMasterModel CreateMasterModel(IPublishedContent content)
		{
			return CreateMasterModel(content, ApplicationContext.Current.Services.DomainService);
		}

		public static IMasterModel CreateMasterModel(IPublishedContent content, IDomainService domainService)
		{
			var baseContent = content as IBase;

			if (baseContent == null)
			{
				throw new Exception(string.Format("{0} does not implement Base doc type", content.DocumentTypeAlias));
			}

			var model = CreateMagicModel(typeof(MasterModel<>), baseContent) as IMasterModel;
			return model;
		}

		private static object CreateMagicModel(Type genericType, IPublishedContent content)
		{
			Type contentType = content.GetType();
			Type modelType = genericType.MakeGenericType(contentType);
			object model = Activator.CreateInstance(modelType, content);

			return model;
		}
	}
}